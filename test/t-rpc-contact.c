#include "gandi.h"

#include <stdlib.h>
#include <stdio.h>

int
main(int argc, char *argv[argc])
{
#ifndef TEST_API_KEY
  printf("\n"
         "================================================================\n"
         "The api key is needed for this test (%s).\n\n"
         "Activate the key with\n"
         "  $ ./configure --enable-test-api-key=<key>\n"
         "================================================================\n",
         argv[0]);

  return EXIT_FAILURE;

#else

  struct rpc rpc;
  char *handle;
  int ret;

  ERROR_RPC(ERR_RPC == rpc_connect(&rpc, TEST_API_KEY, TEST_URL), rpc, "FAIL: rpc_connect()");

  ret = rpc_contact(&rpc, &handle);
  ERROR_RPC(ERR_RPC == ret, rpc, "FAIL: rpc_contact()");
  ERROR_FATAL(0 > ret, "FAIL: rpc_contact()");

  free((void*) handle);

  ERROR_RPC(ERR_RPC == rpc_disconnect(&rpc), rpc, "FAIL: rpc_disconnect()");

  return EXIT_SUCCESS;

#endif
}
