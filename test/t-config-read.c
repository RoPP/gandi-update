#include "gandi.h"

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

#define THE_KEY "Thekey"

int
main(int argc, char *argv[argc])
{
  struct config cfg;
  int ret, status;
  pid_t pid;
  int pipefd[2];

  ERROR_SYS(-1 == pipe(pipefd), ERR_GENERIC, "pipe() failed");

  pid = fork();
  ERROR_SYS(-1 == pid, ERR_GENERIC, "Failed to fork()");

  if (0 == pid)
    {
      close(pipefd[1]);
      close(STDIN_FILENO);
      ERROR_SYS(-1 == dup(pipefd[0]), ERR_GENERIC, "dup(failed)");

      ret = config_read(&cfg, stdin);
      ERROR_FATAL(ret == ERR_FERROR, ERR_GENERIC, "Unable to read file\n");
      ERROR_FATAL(ret == ERR_ANY, ERR_GENERIC, "Failed to parse file\n");

      close(pipefd[0]);

      ERROR_FATAL_FMT( 0 != strcmp(cfg.apikey, THE_KEY), ERR_GENERIC, "The key is %s != %s\n", cfg.apikey, THE_KEY);
      ERROR_FATAL_FMT( 0 != strcmp(cfg.url, TEST_URL), ERR_GENERIC, "The url is %s != %s\n", cfg.url, TEST_URL);

      ERROR_FATAL_FMT( 2 != cfg.nbr_records, ERR_GENERIC, "Number of records is %d != %d\n", cfg.nbr_records, 2);

      ERROR_FATAL_FMT( 0 != strcmp(cfg.record[0].host, "test"), ERR_GENERIC, "The host is %s != %s\n", cfg.record[0].host, "test");
      ERROR_FATAL_FMT( 0 != strcmp(cfg.record[0].domain, "domain"), ERR_GENERIC, "The domain is %s != %s\n", cfg.record[0].domain, "domain");
      ERROR_FATAL_FMT( 0 != strcmp(cfg.record[0].record_type, "A"), ERR_GENERIC, "The domain is %s != %s\n", cfg.record[0].record_type, "A");
      ERROR_FATAL_FMT( 0 != strcmp(cfg.record[0].iface, "eth0"), ERR_GENERIC, "The domain is %s != %s\n", cfg.record[0].iface, "eth0");

      ERROR_FATAL_FMT( 0 != strcmp(cfg.record[1].host, "test2"), ERR_GENERIC, "The host is %s != %s\n", cfg.record[1].host, "test2");
      ERROR_FATAL_FMT( 0 != strcmp(cfg.record[1].domain, "domain2.tld"), ERR_GENERIC, "The domain is %s != %s\n", cfg.record[1].domain, "domain2.tld");
      ERROR_FATAL_FMT( 0 != strcmp(cfg.record[1].record_type, "MX"), ERR_GENERIC, "The domain is %s != %s\n", cfg.record[1].record_type, "MX");
      ERROR_FATAL_FMT( 0 != strcmp(cfg.record[1].iface, "ppp0"), ERR_GENERIC, "The domain is %s != %s\n", cfg.record[1].iface, "ppp0");

      exit(EXIT_SUCCESS);
    }
  else
    {
      FILE *file = fdopen(pipefd[1], "w");
      ERROR_SYS(NULL == file, ERR_GENERIC, "fdopen() failed");
      close(pipefd[0]);

      fprintf(file, "# The key\n");
      fprintf(file, "key %s # this is the key\n", THE_KEY);
      fprintf(file, "url %s\n", TEST_URL);
      fprintf(file, "test.domain A eth0\n");
      fprintf(file, "test2.domain2.tld MX ppp0\n");

      fclose(file);
    }

  while ( -1 == (pid = wait(&status)) && EINTR == errno )
    ;
  ERROR_SYS(-1 == pid, ERR_GENERIC, "wait() failed");
  ERROR_FATAL( (! WIFEXITED(status) ) || EXIT_FAILURE == WEXITSTATUS(status), ERR_GENERIC, "Invalid exit status\n");


  return EXIT_SUCCESS;
}
