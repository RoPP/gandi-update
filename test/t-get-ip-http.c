#include "gandi.h"

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <ctype.h>

int
main(int argc, char *argv[argc])
{
  char ip[NI_MAXHOST];
  int nbr_dot = 0;

  ERROR_FATAL(get_ip_http(ip, "http://myexternalip.com/raw") < 0, ERR_GENERIC, "Curl returned an error");


  for ( char *ptr = ip ; *ptr ; ptr++ )
    {
      if ('.' == *ptr)
        nbr_dot++;
      else
        ERROR_FATAL_FMT(! isdigit(*ptr), ERR_GENERIC, "Invalid char %c", *ptr);
    }

  ERROR_FATAL(3 != nbr_dot, ERR_GENERIC, "Wrong number of dots in ip");

  return EXIT_SUCCESS;
}
