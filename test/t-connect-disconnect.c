#include "gandi.h"

#include <stdlib.h>
#include <stdio.h>

int
main(int argc, char *argv[argc])
{
#ifndef TEST_API_KEY
  printf("\n"
         "================================================================\n"
         "The api key is needed for this test (%s).\n\n"
         "Activate the key with\n"
         "  $ ./configure --enable-test-api-key=<key>\n"
         "================================================================\n",
         argv[0]);

  return EXIT_FAILURE;

#else

  struct rpc rpc;

  ERROR_RPC(ERR_RPC == rpc_connect(&rpc, TEST_API_KEY, TEST_URL), rpc, "FAIL: rpc_connect()");

  ERROR_RPC(ERR_RPC == rpc_disconnect(&rpc), rpc, "FAIL: rpc_disconnect()");

  return EXIT_SUCCESS;

#endif
}
