#ifndef __ERROR_H
#define __ERROR_H

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <syslog.h>
#include <string.h>

#ifndef LOG_PERROR
#define LOG_PERROR 0
#endif

#define ERR_NOT_RUNNING    7
#define ERR_NOT_CONFIGURED 6
#define ERR_NOT_INSTALLED  5
#define ERR_INSUF_PRIV     4
#define ERR_UNIMPLEMENTED  3
#define ERR_INVALID_ARG    2
#define ERR_GENERIC        1

#define ERR_SUCCESS 0
#define ERR_ERRNO  -1
#define ERR_RPC    -2
#define ERR_FERROR -3
#define ERR_GAI    -4
#define ERR_ANY    -16
#define ERR_LAST   ERR_ANY

#define ERROR_SYS_FMT(test, error_code, fmt, ...)       \
  do                                                    \
    {                                                   \
      if (test)                                         \
        {                                               \
          fprintf(stderr, fmt, __VA_ARGS__);            \
          fprintf(stderr, "%s\n", strerror(errno));     \
          exit(error_code);                             \
        }                                               \
    } while (0)

#define ERROR_SYS(test, error_code, str)                \
  do                                                    \
    {                                                   \
      if (test)                                         \
        {                                               \
          perror(str);                                  \
          exit(error_code);                             \
        }                                               \
    } while (0)

#define ERROR_FATAL(test, error_code, str)              \
  do                                                    \
    {                                                   \
      if (test)                                         \
        {                                               \
          fprintf(stderr, "%s\n", str);                 \
          exit(error_code);                             \
        }                                               \
    }                                                   \
  while (0)

#define ERROR_FATAL_FMT(test, error_code, fmt, ...)     \
  do                                                    \
    {                                                   \
      if (test)                                         \
        {                                               \
          fprintf(stderr, fmt, __VA_ARGS__);            \
          exit(error_code);                             \
        }                                               \
    } while (0)

#define ERROR_MSG(test,str)                             \
  do                                                    \
    {                                                   \
      if (test)                                         \
        {                                               \
          fprintf(stderr, "%s\n", str);                 \
        }                                               \
    }                                                   \
  while (0)

#define ERROR_MSG_FMT(test,fmt,...)                     \
  do                                                    \
    {                                                   \
      if (test)                                         \
        {                                               \
          fprintf(stderr, fmt, __VA_ARGS__);            \
        }                                               \
    } while (0)



#define LOG_GEN(test,level,str)                         \
  do                                                    \
    {                                                   \
      if (test)                                         \
        syslog(level, "%s\n", str);                     \
    } while (0)

#define LOG_GEN_FMT(test,level,fmt,...)                 \
  do                                                    \
    {                                                   \
      if (test)                                         \
        syslog(LOG_ERR, fmt, __VA_ARGS__);              \
    } while (0)

#define LOG_SYS_FMT(test,fmt,...) LOG_GEN_FMT(test,level, fmt": %s", __VA_ARGS__, strerror(errno))
#define LOG_SYS(test,level,str) LOG_GEN_FMT(test,level,"%s: %s", str, strerror(errno))


#endif
