#ifndef __PARAMS_H
#define __PARAMS_H

#define MAX_KEY_SIZE     25
#define MAX_URL_SIZE     2048

typedef char api_key_t[MAX_KEY_SIZE];
typedef char url_t[MAX_URL_SIZE];

int set_key(char *dst, char *src);
int set_url(char *dst, char *src);

#endif
