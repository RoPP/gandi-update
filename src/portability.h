#ifndef __PORTABILITY_H
#define __PORTABILITY_H

#include <limits.h>
#include <netdb.h>

#ifndef PATH_MAX
# define PATH_MAX 4096
#endif

#ifndef NI_MAXHOST
#define NI_MAXHOST 1025
#endif

#endif
