#ifndef __NET_UTILS_H
#define __NET_UTILS_H


#define get_ip_iface get_ip_iface_linux

int get_ip(char *ip, char *handle);

int get_ip_iface_linux(char *ip, char *iface);
int get_ip_http(char *ip, char *url);

#endif
