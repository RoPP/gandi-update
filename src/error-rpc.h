#ifndef __ERROR_RPC_H
#define __ERROR_RPC_H


#define ERROR_RPC_FMT(test,rpc,fmt,...)                                 \
  do                                                                    \
    {                                                                   \
      xmlrpc_env *env = &(rpc).env;                                     \
      if ( (test) && env->fault_occurred )                              \
        {                                                               \
          fprintf(stderr, fmt, __VA_ARGS__);                            \
          ERROR_FATAL_FMT(1,"%s (%d)\n", env->fault_string,             \
                          env->fault_code);                             \
        }                                                               \
    } while (0)

#define ERROR_RPC(test,rpc,str)                                         \
  do                                                                    \
    {                                                                   \
      xmlrpc_env *env = &(rpc).env;                                     \
      if ( (test) && env->fault_occurred )                              \
        {                                                               \
          ERROR_FATAL_FMT(1, "%s: %s (%d)\n", str, env->fault_string,   \
                          env->fault_code);                             \
        }                                                               \
    } while (0)

#define LOG_RPC_FMT(test,level,rpc,fmt,...)                             \
  do {                                                                  \
    LOG_GEN_FMT(test,level, fmt": %s (%d)", __VA_ARGS__,                \
                (rpc).env.fault_string,                                 \
                (rpc).env.fault_code);                                  \
  } while (0)

#define LOG_RPC(test,level,rpc,str) LOG_RPC_FMT(test,level,rpc, "%s:", str)

#endif
