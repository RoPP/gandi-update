#include "gandi.h"

#include <ctype.h>
#include <string.h>

#define SKIP_SPACES(str)                        \
  while ( isspace( *str ) )                     \
    {                                           \
      str++;                                    \
    }

#define SKIP_LINE(str)                          \
  while ( *str && *str != '\n' )                \
    {                                           \
      str++;                                    \
    }


#define NEXT_TOKEN(buf,token)                                           \
  do {                                                                  \
    char *t_end;                                                        \
    int size = 0;                                                       \
    (token) = (buf);                                                    \
                                                                        \
    SKIP_SPACES(token);                                                 \
                                                                        \
    if (0 == *(token))                                                  \
      break;                                                            \
                                                                        \
    for ( t_end = (token) ; *t_end && ! isspace(*t_end) ; t_end++ )     \
      size++;                                                           \
                                                                        \
    if (0 != *t_end)                                                    \
      size++;                                                           \
    *t_end = 0;                                                         \
                                                                        \
    (buf) = (token) + size;                                             \
  } while (0)

#define SET_TOKEN(to, buf)                      \
  do {                                          \
    char *token;                                \
                                                \
    NEXT_TOKEN((buf),token);                    \
    (to) = token;                               \
  } while (0)

int
config_read(struct config *cfg, FILE *input)
{
  char *buf = cfg->buffer;

  cfg->buf_size = fread(cfg->buffer, sizeof(char), MAX_BUFFER_SIZE, input);
  if ( ferror(input) )
    {
      return ERR_FERROR;
    }

  cfg->buffer[cfg->buf_size] = 0;
  cfg->nbr_records = 0;

  while (*buf)
    {
      char *token;

      NEXT_TOKEN(buf,token);
      if ( 0 == *token)
        break;

      if ( '#' == *token )
        {
          SKIP_LINE(buf);
        }
      else if ( 0 == strcmp("key", token) )
        {
          SET_TOKEN(cfg->apikey, buf);
        }
      else if ( 0 == strcmp("url", token) )
        {
          SET_TOKEN(cfg->url, buf);
        }
      else
        {
          int nbr = cfg->nbr_records;
          char *point;

          if (MAX_NBR_RECORD <= nbr)
            return ERR_ANY;

          point = strchr(token, '.');
          if (NULL == point)
            return ERR_ANY;

          *point = 0;

          cfg->record[nbr].host = token;
          cfg->record[nbr].domain = point+1;
          SET_TOKEN(cfg->record[nbr].record_type, buf);
          SET_TOKEN(cfg->record[nbr].iface, buf);

          cfg->nbr_records++;
        }
    }
  if (cfg->nbr_records == 0)
    {
      return ERR_NOT_CONFIGURED;
    }

  return ERR_SUCCESS;
}

