#include "gandi.h"

#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>

#include <curl/curl.h>

int
get_ip(char *ip, char *handle)
{
#define HTTP_PREFIX "http://"
  if ( 0 == strncmp(handle, HTTP_PREFIX, strlen(HTTP_PREFIX)) )
    return get_ip_http(ip, handle);

  return get_ip_iface(ip, handle);
}

int
get_ip_iface_linux(char *ip, char *iface)
{
  struct ifaddrs *ifaddr, *ifa;
  int family, s;
  char host[NI_MAXHOST];
  int ret = ERR_ANY;

  if (-1 == getifaddrs(&ifaddr))
    return ERR_ERRNO;

  for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
    if (ifa->ifa_addr == NULL)
      continue;

    family = ifa->ifa_addr->sa_family;

    if ( family == AF_INET && 0 == strcmp(iface, ifa->ifa_name) )
      {
        s = getnameinfo(ifa->ifa_addr,
                        (family == AF_INET) ? sizeof(struct sockaddr_in) :
                        sizeof(struct sockaddr_in6),
                        host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);
        if (s != 0)
          return ERR_GAI; // printf("getnameinfo() failed: %s\n", gai_strerror(s));

        strcpy(ip, host);
        ret = ERR_SUCCESS;

        break;
      }
  }

  freeifaddrs(ifaddr);

  return ret;
}

struct curl_data {
  char host[NI_MAXHOST];
  int len;
  int status;
};

static
size_t
write_func(char *ptr, size_t size, size_t nmemb, void *userdata)
{
  struct curl_data *cd = userdata;

  for (int i = 0 ; i < size * nmemb && cd->len < NI_MAXHOST-1 ; i++)
    {
      char c = ptr[i];

      switch (cd->status)
        {
        case 0:
          if (isdigit(c))
            {
              cd->host[cd->len++] = c;
              cd->status = 1;
            }
          else if (! isspace(c))
            return 0;

          break;


        case 1:
        case 2:
        case 3:
          if (isdigit(c) || '.' == c)
            cd->host[cd->len++] = c;
          else
            return 0;

          if ( '.' == c)
            cd->status++;

          break;

        case 4:
          if (isdigit(c))
            cd->host[cd->len++] = c;
          else if (isspace(c))
            cd->status = 5;
          else
            return 0;

          break;

        default:
          return 0;
        }
    }

  return size * nmemb;
}

int
get_ip_http(char *ip, char *url)
{
  CURL *curl;
  CURLcode res;
  struct curl_data cd = { .host = {0}, .len = 0, .status = 0 };

  curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, url); // "http://myexternalip.com/raw"
    curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1L);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_func);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &cd);

    res = curl_easy_perform(curl);

    curl_easy_cleanup(curl);

    if(res != CURLE_OK)
      return ERR_ANY;
   }

  strcpy(ip, cd.host);

  return ERR_SUCCESS;
}
