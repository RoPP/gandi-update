#include "gandi.h"

#include <string.h>

int
set_key(char *dst, char *src)
{
  strncpy(dst, src, MAX_KEY_SIZE);
  dst[MAX_KEY_SIZE-1] = '\0';

  return 0;
}

int
set_url(char *dst, char *src)
{
  strncpy(dst, src, MAX_URL_SIZE);
  dst[MAX_URL_SIZE-1] = '\0';

  return 0;
}
