#ifndef __OPTIONS_H
#define __OPTIONS_H

#include <sys/types.h>

#include "portability.h"
#include "parse-config.h"

#define TEST_URL   "https://rpc.ote.gandi.net/xmlrpc/"
#define DEFAULT_URL TEST_URL
#define PROD_URL "https://rpc.gandi.net/xmlrpc/"

struct options {
  struct config cfg;
  int daemon;
  int once;
  unsigned int delay;
  char pidfile[PATH_MAX+1];
  uid_t uid;
  gid_t gid;
};

void options_handle(struct options *opt, int argc, char *argv[ ]);

#endif
