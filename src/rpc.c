#include "gandi.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>


#define CLIENT_NAME             "Gandi-update"
#define CLIENT_VERSION          VERSION
#define CLIENT_USERAGENT        CLIENT_NAME "/" CLIENT_VERSION

int
rpc_connect(struct rpc *rpc, char *apikey, char *url)
{
  struct xmlrpc_clientparms clientp;
  struct xmlrpc_curl_xportparms curlp;
  xmlrpc_env *env = &rpc->env;

  set_key(rpc->apikey, apikey);
  set_url(rpc->url, url);

  curlp.network_interface = NULL;         /* use curl's default */
  curlp.no_ssl_verifypeer = 0;               /* Gandi API CA must be present */
  curlp.no_ssl_verifyhost = 0;
  curlp.user_agent = CLIENT_USERAGENT;    /* XML-RPC requirement */

  clientp.transport = "curl";
  clientp.transportparmsP = &curlp;
  clientp.transportparm_size = XMLRPC_CXPSIZE(user_agent);

  xmlrpc_env_init(env);
  if (env->fault_occurred)
    return ERR_RPC;

  xmlrpc_client_init2(env, XMLRPC_CLIENT_NO_FLAGS, CLIENT_NAME,
                      CLIENT_VERSION, &clientp, XMLRPC_CPSIZE(transportparm_size));
  if (env->fault_occurred)
    return ERR_RPC;

  return ERR_SUCCESS;
}

int
rpc_disconnect(struct rpc *rpc)
{
  xmlrpc_env_clean(&rpc->env);
  xmlrpc_client_cleanup();

  return ERR_SUCCESS;
}

void
rpc_env_reset_if_fault(struct rpc *rpc)
{
  xmlrpc_env *env = &rpc->env;

  if (rpc->env.fault_occurred)
    {
      xmlrpc_env_clean(env);
      xmlrpc_env_init(env);
    }
}

int
rpc_struct_read_string(struct rpc *rpc, xmlrpc_value *v, char *key, char **value)
{
  xmlrpc_env *env = &rpc->env;
  xmlrpc_value *structv;

  rpc_env_reset_if_fault(rpc);

  xmlrpc_struct_find_value(env, v, key, &structv);
  if (env->fault_occurred)
    return ERR_RPC;

  if (structv)
    {
      xmlrpc_read_string(env, structv, (const char **) value);
      xmlrpc_DECREF(structv);
      if (env->fault_occurred)
        return ERR_RPC;
    }
  else
    return ERR_ANY;

  return ERR_SUCCESS;
}

int
rpc_struct_read_int(struct rpc *rpc, xmlrpc_value *v, char *key, int * value)
{
  xmlrpc_env *env = &rpc->env;
  xmlrpc_value *structv;

  rpc_env_reset_if_fault(rpc);

  xmlrpc_struct_find_value(env, v, key, &structv);
  if (env->fault_occurred)
    return ERR_RPC;

  if (structv)
    {
      xmlrpc_read_int(env, structv, value);
      xmlrpc_DECREF(structv);
      if (env->fault_occurred)
        return ERR_RPC;
    }
  else
    return ERR_ANY;

  return ERR_SUCCESS;
}

int
rpc_array_struct_read_string(struct rpc *rpc, xmlrpc_value *v, int index, char *key, char **value)
{
  xmlrpc_env *env = &rpc->env;
  xmlrpc_value *structv;
  int ret = ERR_SUCCESS;

  rpc_env_reset_if_fault(rpc);

  xmlrpc_array_read_item(env, v, index, &structv);
  if (env->fault_occurred)
    return ERR_RPC;

  if (structv)
    {
      ret = rpc_struct_read_string(rpc, structv, key, value);
      xmlrpc_DECREF(structv);
    }
  else
    return ERR_ANY;

  return ret;
}

int
rpc_array_struct_read_int(struct rpc *rpc, xmlrpc_value *v, int index, char *key, int *value)
{
  xmlrpc_env *env = &rpc->env;
  xmlrpc_value *structv;
  int ret = ERR_SUCCESS;

  rpc_env_reset_if_fault(rpc);

  xmlrpc_array_read_item(env, v, index, &structv);
  if (env->fault_occurred)
    return ERR_RPC;

  if (structv)
    {
      ret = rpc_struct_read_int(rpc, structv, key, value);
      xmlrpc_DECREF(structv);
    }
  else
    return ERR_ANY;

  return ret;
}

int
rpc_api_version(struct rpc *rpc, char **version)
{
  xmlrpc_env *env = &rpc->env;
  xmlrpc_value *rv;
  int ret;

  rpc_env_reset_if_fault(rpc);

  rv = xmlrpc_client_call(env, rpc->url, "version.info", "(s)", rpc->apikey);
  if (env->fault_occurred)
    return ERR_RPC;

  ret = rpc_struct_read_string(rpc, rv, "api_version", version);

  xmlrpc_DECREF(rv);

  if ( ret < 0 )
    return ret;

  return ERR_SUCCESS;
}

void
rpc_dump_value(struct rpc *rpc, xmlrpc_value *rv)
{
  xmlrpc_env *env = &rpc->env;
  xmlrpc_mem_block mb;

  rpc_env_reset_if_fault(rpc);

  XMLRPC_MEMBLOCK_INIT(char, env, &mb, 0);

  xmlrpc_serialize_value(env, &mb, rv);

  printf("%s\n", XMLRPC_MEMBLOCK_CONTENTS(char, &mb));

  XMLRPC_MEMBLOCK_CLEAN(char, &mb);
}
