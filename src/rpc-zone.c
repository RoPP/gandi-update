#include "gandi.h"

#include <stdio.h>
#include <stdlib.h>

int
rpc_zone_id(struct rpc *rpc, char *domain)
{
  xmlrpc_env *env = &rpc->env;
  xmlrpc_value *rv;
  int zone_id, ret;

  rpc_env_reset_if_fault(rpc);

  rv = xmlrpc_client_call(env, rpc->url, "domain.info",  "(ss)", rpc->apikey, domain);
  if (env->fault_occurred)
    return ERR_RPC;

  ret = rpc_struct_read_int(rpc, rv, "zone_id", &zone_id);

  xmlrpc_DECREF(rv);

  if ( ret < 0 )
    return ret;

  return zone_id;
}

int
rpc_current_zone_version(struct rpc *rpc, int zone_id)
{
  xmlrpc_env *env = &rpc->env;
  xmlrpc_value *rv;
  int version, ret;

  rpc_env_reset_if_fault(rpc);

  rv = xmlrpc_client_call(env, rpc->url, "domain.zone.info",  "(si)", rpc->apikey, zone_id);
  if (env->fault_occurred)
    return ERR_RPC;

  ret = rpc_struct_read_int(rpc, rv, "version", &version);

  xmlrpc_DECREF(rv);

  if ( ret < 0 )
    return ret;

  return version;
}

int
rpc_new_zone_version(struct rpc *rpc, int zone_id)
{
  xmlrpc_env *env = &rpc->env;
  xmlrpc_value *rv;
  int new_version_id;

  rpc_env_reset_if_fault(rpc);

  rv = xmlrpc_client_call(env, rpc->url, "domain.zone.version.new",  "(si)", rpc->apikey, zone_id);
  if (env->fault_occurred)
    return ERR_RPC;

  xmlrpc_read_int(env, rv, &new_version_id);
  xmlrpc_DECREF(rv);
  if (env->fault_occurred)
    return ERR_RPC;

  return new_version_id;
}

int
rpc_delete_zone_record(struct rpc *rpc, int zone_id, int version_id, char *name, char *type)
{
  xmlrpc_env *env = &rpc->env;
  xmlrpc_value *rv;

  rpc_env_reset_if_fault(rpc);

  rv = xmlrpc_client_call(env, rpc->url, "domain.zone.record.delete",  "(sii{s:s,s:s})",
                          rpc->apikey, zone_id, version_id,
                          "name", name, "type", type);
  if (env->fault_occurred)
    return ERR_RPC;

  xmlrpc_DECREF(rv);

  return ERR_SUCCESS;
}

int
rpc_add_zone_record(struct rpc *rpc, int zone_id, int version_id, char *name, char *type, char *value, int ttl)
{
  xmlrpc_env *env = &rpc->env;
  xmlrpc_value *rv;
  int real_ttl = (ttl < 300)?300:ttl;

  rpc_env_reset_if_fault(rpc);

  rv = xmlrpc_client_call(env, rpc->url, "domain.zone.record.add",  "(sii{s:s,s:s,s:s,s:i})",
                          rpc->apikey, zone_id, version_id,
                          "name", name, "type", type, "value", value, "ttl", real_ttl);
  if (env->fault_occurred)
    return ERR_RPC;

  xmlrpc_DECREF(rv);

  return ERR_SUCCESS;
}

int
rpc_value_zone_record(struct rpc *rpc, int zone_id, int version_id, char *name, char *type,
                      char **value)
{
  xmlrpc_env *env = &rpc->env;
  xmlrpc_value *rv;
  int ret, id;

  rpc_env_reset_if_fault(rpc);

  rv = xmlrpc_client_call(env, rpc->url, "domain.zone.record.list",  "(sii{s:s,s:s})",
                          rpc->apikey, zone_id, version_id,
                          "name", name, "type", type);
  if (env->fault_occurred)
    return ERR_RPC;

  ret = rpc_array_struct_read_int(rpc, rv, 0, "id", &id);
  if (ret >= 0)
    ret = rpc_array_struct_read_string(rpc, rv, 0, "value", value);
  xmlrpc_DECREF(rv);
  if (ret < 0)
    return ret;

  return id;
}

int
rpc_update_zone_record(struct rpc *rpc, int zone_id, int version_id, int record_id,
                       char *name, char *type, char *value, int ttl)
{
  xmlrpc_env *env = &rpc->env;
  xmlrpc_value *rv;
  int real_ttl = (ttl < 300)?300:ttl;

  rpc_env_reset_if_fault(rpc);

  rv = xmlrpc_client_call(env, rpc->url, "domain.zone.record.update",  "(sii{s:i}{s:s,s:s,s:s,s:i})",
                          rpc->apikey, zone_id, version_id,
                          "id", record_id,
                          "name", name, "type", type, "value", value, "ttl", real_ttl);
  if (env->fault_occurred)
    return ERR_RPC;

  xmlrpc_DECREF(rv);

  return ERR_SUCCESS;
}

int
rpc_zone_record_exists(struct rpc *rpc, int zone_id, int version_id, char *name, char *type)
{
  xmlrpc_env *env = &rpc->env;
  xmlrpc_value *rv;
  int count = 0;

  rpc_env_reset_if_fault(rpc);

  rv = xmlrpc_client_call(env, rpc->url, "domain.zone.record.count",  "(sii{s:s,s:s})",
                          rpc->apikey, zone_id, version_id,
                          "name", name, "type", type);
  if (env->fault_occurred)
    return ERR_RPC;

  xmlrpc_read_int(env, rv, &count);
  xmlrpc_DECREF(rv);
  if (env->fault_occurred)
    return ERR_RPC;

  return count;
}

int
rpc_activate_zone_version(struct rpc *rpc, int zone_id, int version_id)
{
  xmlrpc_env *env = &rpc->env;
  xmlrpc_value *rv;

  rpc_env_reset_if_fault(rpc);

  rv = xmlrpc_client_call(env, rpc->url, "domain.zone.version.set",  "(sii)", rpc->apikey, zone_id, version_id);
  if (env->fault_occurred)
    return ERR_RPC;

  xmlrpc_DECREF(rv);

  return ERR_SUCCESS;
}


int
rpc_delete_zone_version(struct rpc *rpc, int zone_id, int version_id)
{
  xmlrpc_env *env = &rpc->env;
  xmlrpc_value *rv;

  rpc_env_reset_if_fault(rpc);

  rv = xmlrpc_client_call(env, rpc->url, "domain.zone.version.delete",  "(sii)",
                          rpc->apikey, zone_id, version_id);
  if (env->fault_occurred)
    return ERR_RPC;

  xmlrpc_DECREF(rv);

  return ERR_SUCCESS;
}
