#include "gandi.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>

#define USLEEP(time) do { for (unsigned int remain = time ; 0 < remain ; remain = sleep(remain)); } while (0)

static int connection_established = 0;

void
cond_connect(struct rpc *rpc, struct options *opt)
{
  int ret;
  unsigned int retry = 60;
#define MAX_RETRY 3600

  if ( ! connection_established )
    {
      do {
        ret = rpc_connect(rpc, opt->cfg.apikey, opt->cfg.url);
        LOG_RPC(ERR_RPC == ret, LOG_ERR, *rpc, "Failed to connect");
        if (ret < 0)
          {
            USLEEP(retry);
            retry *= 2;
            if (MAX_RETRY < retry)
              retry = MAX_RETRY;
          }
      } while (ret < 0);

      connection_established = 1;
    }
}

void
cond_disconnect(struct rpc *rpc)
{
  if (connection_established)
    {
      rpc_disconnect(rpc);
      connection_established = 0;
    }
}

void
array_to_null(char *arr[], int n)
{
  for (int i = 0 ;  i < n ; i++)
    arr[i] = NULL;
}

void
array_free(char *arr[], int n)
{
  for (int i = 0 ;  i < n ; i++)
    if ( NULL != arr[i] )
      free(arr[i]);
}

void
quit()
{
  closelog();

  exit(EXIT_SUCCESS);
}

int
main(int argc, char **argv)
{
  struct rpc rpc;
  struct options opt;
  int ret;

  options_handle(&opt, argc, argv);

  signal(SIGTERM, quit);

  if(opt.daemon)
    {
      ERROR_SYS(ERR_ERRNO == daemon_init(opt.pidfile), ERR_GENERIC, "unable to initialize daemon");
    }

  if(-1 != opt.uid || -1 != opt.gid)
    {
      ERROR_SYS(ERR_ERRNO == drop_privileges(opt.uid, opt.gid),
        ERR_INSUF_PRIV,
        "drop_privileges(): unable to drop privileges");
    }

  openlog("gandi-update", (opt.daemon?0:LOG_PERROR) | LOG_CONS | LOG_PID, LOG_DAEMON);

  char *last_value[opt.cfg.nbr_records];

  array_to_null(last_value, opt.cfg.nbr_records);

  // daemon loop
  do
    {
      // Loop on different domains
      for (int i = 0 ;  i < opt.cfg.nbr_records ; )
        {
          char *last_domain = opt.cfg.record[i].domain;
          int new_version_created = 0;
          struct update u;

          // loop on one consecutive record of a domain
          for (  ; i <  opt.cfg.nbr_records &&
                   0 == strcmp(last_domain, opt.cfg.record[i].domain) ;
                 i++)
            {
              char ip[NI_MAXHOST];

              LOG_GEN_FMT(1, LOG_INFO, "Checking update for %s.%s %s %s",
                          opt.cfg.record[i].host, opt.cfg.record[i].domain,
                          opt.cfg.record[i].record_type, opt.cfg.record[i].iface);

              ret = get_ip(ip, opt.cfg.record[i].iface);
              LOG_GEN(ret < 0, LOG_ERR, "getip() failed");
              if (ret < 0)
                continue;

              if ( NULL == last_value[i])
                {
                  cond_connect(&rpc, &opt);

                  if (0 == new_version_created)
                    if ( update_get_version(&rpc, opt.cfg.record[i].domain, &u) < 0 )
                      continue;

                  update_get_current_value(&rpc, &u, opt.cfg.record[i].host, opt.cfg.record[i].record_type,
                                           &last_value[i]);
                }

              if ( NULL != last_value[i] &&
                   0 == strcmp(ip, last_value[i]) )
                {
                  LOG_GEN_FMT(1, LOG_INFO, "Nothing to do for %s.%s %s %s",
                          opt.cfg.record[i].host, opt.cfg.record[i].domain,
                          opt.cfg.record[i].record_type, ip);

                  continue;
                }

              cond_connect(&rpc, &opt);

              if ( ! new_version_created )
                {
                  ret = update_new_version(&rpc, opt.cfg.record[i].domain, &u);
                  if (ret < 0)
                    continue;

                  new_version_created = 1;
                }

              if ( update_set_record(&rpc, &u, opt.cfg.record[i].host, opt.cfg.record[i].record_type, ip, 300) < 0 )
                continue;

              if (last_value[i])
                free(last_value[i]);

              last_value[i] = strdup(ip);
              LOG_SYS(NULL == last_value[i], LOG_EMERG, "Exiting: strdup() failed");
              if (NULL == last_value[i])
                exit(EXIT_FAILURE);

              LOG_GEN_FMT(1, LOG_INFO, "Record set to %s.%s %s %s",
                          opt.cfg.record[i].host, opt.cfg.record[i].domain,
                          opt.cfg.record[i].record_type, ip);
            }

          if (new_version_created)
            update_activate_version(&rpc, &u);

        }

      cond_disconnect(&rpc);

      if (!opt.once)
        USLEEP(opt.delay);
    }
  while (!opt.once);

  array_free(last_value, opt.cfg.nbr_records);

  quit();
}
