#include "gandi.h"

#include <getopt.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>


enum {
  OPT_VERSION = 0,
  OPT_CONFIG,
  OPT_DAEMON,
  OPT_ONCE,
  OPT_PIDFILE,
  OPT_USER,
  OPT_GROUP,
  OPT_DELAY
};

#define DEFAULT_DELAY 300
#define DEFAULT_CONF "/etc/gandi-update.conf"
#define DEFAULT_PIDFILE "/var/run/gandi-update.pid"

static void
print_version()
{
  printf("%s\n", VERSION);
}

static void
print_help()
{
  printf("Usage: gandi-update [options]\n\n");

  printf("Options:\n");
  printf("     --help, -h           this help\n"
         "     --version            show version numbers\n"
         "\n"
         "     --config <name>      The configuration filename (Default: %s)\n"
         "\n"
         "     --daemon             Activate daemon mode\n"
         "     --once               Update once\n"
         "     --delay <sec>        The delay between checks (Default: %d)\n"
         "\n"
         "     --pidfile <path>     The path to the pid file (Default: %s)\n"
         "\n"
         "     --user <name>        Change the user of the process\n"
         "     --group <name>       Change the group of the process\n",
         DEFAULT_CONF,
         DEFAULT_DELAY,
         DEFAULT_PIDFILE
         );

  printf("\n");
}

void
options_handle(struct options *opt, int argc, char *argv[ ])
{
  int c, ret;
  FILE *file;
  struct passwd *pwd;
  struct group *grp;
  char *config_fn = strdup(DEFAULT_CONF);
  ERROR_SYS(NULL == config_fn, ERR_GENERIC, "strdup() failed");

  opt->cfg = (struct config) { .url = TEST_URL,
                               .apikey = "",
                               .nbr_records = 0  };

  opt->daemon = 0;
  opt->once = 0;
  opt->delay = DEFAULT_DELAY;
  strcpy(opt->pidfile, DEFAULT_PIDFILE);
  opt->uid = opt->gid = -1;

  while (1) {
    int option_index = 0;
    static struct option long_options[] = {
      {"help", 0, 0, 'h'},
      {"version", 0, 0, OPT_VERSION},
      {"config", 1, 0, OPT_CONFIG},
      {"daemon", 0, 0, OPT_DAEMON},
      {"once", 0, 0, OPT_ONCE},
      {"delay", 1, 0, OPT_DELAY},
      {"pidfile", 1, 0, OPT_PIDFILE},
      {"user", 1, 0, OPT_USER},
      {"group", 1, 0, OPT_GROUP},
      {0, 0, 0, 0}
    };

    c = getopt_long (argc, argv, "h",
                     long_options, &option_index);
    if (c == -1)
      break;

    switch (c)
      {
      case OPT_VERSION:
        print_version();
        exit(0);
        break;

      case OPT_CONFIG:
        config_fn = strdup(optarg);
        ERROR_SYS(NULL == config_fn, ERR_GENERIC, "strdup() failed");

        break;

      case OPT_DAEMON:
        opt->daemon = 1;
        break;

      case OPT_ONCE:
        opt->once = 1;
        break;

      case OPT_DELAY:
        opt->delay = strtol(optarg, (char **) NULL, 10);
        if (opt->delay < 30)
          opt->delay = 30;
        break;

      case OPT_PIDFILE:
        strncpy(opt->pidfile, optarg, PATH_MAX);
        opt->pidfile[PATH_MAX] = 0;
        break;

      case OPT_USER:
        errno = 0;
        pwd = getpwnam(optarg);
        if ( NULL == pwd )
          ERROR_SYS(0 != errno, ERR_GENERIC, "getpwnam() failed");
        else
          {
            opt->uid = pwd->pw_uid;
            if (-1 == opt->gid)
              opt->gid = pwd->pw_gid;
          }
        break;

      case OPT_GROUP:
        errno = 0;
        grp = getgrnam(optarg);
        if ( NULL == grp )
          ERROR_SYS(0 != errno, ERR_GENERIC, "getgrnam() failed");
        else
          opt->gid = grp->gr_gid;
        break;

      case 'h':
        print_help();
        ERROR_FATAL(1, ERR_SUCCESS, "");
        break;

      default:
        print_help();
        ERROR_FATAL_FMT(1, ERR_INVALID_ARG, "?? getopt returned character code 0%o ??\n", c);
      }
  }

  file = fopen(config_fn, "r");
  ERROR_SYS_FMT(NULL == file, ERR_NOT_CONFIGURED, "Unable to open config file %s\n", config_fn);
  free(config_fn);

  ret = config_read(&opt->cfg, file);
  ERROR_FATAL(ret == ERR_FERROR, ERR_NOT_CONFIGURED, "Unable to read config file\n");
  ERROR_FATAL(ret == ERR_NOT_CONFIGURED, ERR_NOT_CONFIGURED, "No records in config file\n");
  ERROR_FATAL(0 > ret, ERR_NOT_CONFIGURED, "Unable to parse config file\n");

  fclose(file);
}
