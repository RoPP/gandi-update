#include "gandi.h"

#include <stdio.h>
#include <stdlib.h>


int
rpc_add_domain(struct rpc *rpc, char *domain,
               char *admin, char *bill, int duration, char *owner, char *tech)
/*
admin           string  yes Administrative contact handle
bill            string  yes Billing contact handle
duration        int     yes Registration period in years, between 1 and 10
owner           string  yes Registrant handle
tech            string  yes Technical contact handle
*/
{
  xmlrpc_env *env = &rpc->env;
  xmlrpc_value *rv;

  rpc_env_reset_if_fault(rpc);

  rv = xmlrpc_client_call(env, rpc->url, "domain.create",  "(ss{s:s,s:s,s:i,s:s,s:s})",
                          rpc->apikey, domain,
                          "admin", admin,
                          "bill", bill,
                          "duration", duration,
                          "owner", owner,
                          "tech", tech);

  if (env->fault_occurred)
    return ERR_RPC;

  // rpc_dump_value(rpc, rv)  ;

  xmlrpc_DECREF(rv);

  return ERR_SUCCESS;
}
