#include "gandi.h"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>



int
drop_privileges(uid_t uid, gid_t gid)
{
  if (-1 == setregid(gid, gid))
    return ERR_ERRNO;

  if (-1 == setreuid(uid, uid))
    return ERR_ERRNO;

  return ERR_SUCCESS;
}

static
int
write_pid(char *pidfile)
{
  pid_t pid = getpid();
  FILE *file;

  file = fopen(pidfile, "w");
  if (NULL == file)
    return ERR_ERRNO;

  fprintf(file, "%d", pid);

  if ( 0 != fclose(file) )
    return ERR_ERRNO;

  return ERR_SUCCESS;
}

/* Stevens Advanced Unix Programming in the UNIX Environment */
int
daemon_init(char *pidfile)
{
  pid_t pid;
  int ret;

  pid = fork();

  if (pid < 0)
    return ERR_ERRNO;

  if (pid != 0)
    exit(EXIT_SUCCESS);

  setsid();

  ret = chdir("/");
  if (ret < 0)
    return ret;

  umask(0);

  ret = write_pid(pidfile);
  if (ret < 0)
    return ret;

  return ERR_SUCCESS;
}
