#ifndef __RPC_H
#define __RPC_H

#include <xmlrpc.h>
#include <xmlrpc_client.h>

#include "params.h"

struct rpc {
  xmlrpc_env env;
  api_key_t apikey;
  url_t url;
};

int rpc_connect(struct rpc *rpc, char *apikey, char *url);
int rpc_disconnect(struct rpc *rpc);

void rpc_dump_value(struct rpc *rpc, xmlrpc_value *rv);

int rpc_api_version(struct rpc *rpc, char **version);
int rpc_zone_id(struct rpc *rpc, char *domain);
int rpc_current_zone_version(struct rpc *rpc, int zone_id);
int rpc_new_zone_version(struct rpc *rpc, int zone_id);

int rpc_delete_zone_record(struct rpc *rpc, int zone_id, int version_id, char *name, char *type);
int rpc_add_zone_record(struct rpc *rpc, int zone_id, int version_id,
                        char *name, char *type, char *value, int ttl);
int rpc_value_zone_record(struct rpc *rpc, int zone_id, int version_id, char *name, char *type,
                          char **value);
int rpc_update_zone_record(struct rpc *rpc, int zone_id, int version_id, int record_id,
                           char *name, char *type, char *value, int ttl);
int rpc_zone_record_exists(struct rpc *rpc, int zone_id, int version_id, char *name, char *type);
int rpc_activate_zone_version(struct rpc *rpc, int zone_id, int version_id);
int rpc_delete_zone_version(struct rpc *rpc, int zone_id, int version_id);

int rpc_contact(struct rpc *rpc, char **contact);
int rpc_add_domain(struct rpc *rpc, char *domain,
                   char *admin, char *bill, int duration, char *owner, char *tech);

// Read
int rpc_struct_read_string(struct rpc *rpc, xmlrpc_value *v, char *key, char **value);
int rpc_struct_read_int(struct rpc *rpc, xmlrpc_value *v, char *key, int * value);
int rpc_array_struct_read_string(struct rpc *rpc, xmlrpc_value *v, int index, char *key, char **value);
int rpc_array_struct_read_int(struct rpc *rpc, xmlrpc_value *v, int index, char *key, int *value);

void rpc_env_reset_if_fault(struct rpc *rpc);

#endif
