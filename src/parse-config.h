#ifndef __CONFIG_H
#define __CONFIG_H

#include <stdio.h>

#define MAX_BUFFER_SIZE (1ll << 17)
#define MAX_NBR_RECORD (1ll << 10)

struct config_record {
  char *host;
  char *domain;
  char *record_type;
  char *iface;
};

struct config {
  char buffer[MAX_BUFFER_SIZE+1];
  size_t buf_size;

  char *apikey;
  char *url;

  struct config_record record[MAX_NBR_RECORD];
  int nbr_records;
};

int config_read(struct config *cfg, FILE *input);

#endif
