#include "gandi.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

int
rpc_contact(struct rpc *rpc, char **contact)
{
  xmlrpc_env *env = &rpc->env;
  xmlrpc_value *rv;
  int ret;

  rpc_env_reset_if_fault(rpc);

  rv = xmlrpc_client_call(env, rpc->url, "contact.info", "(s)", rpc->apikey);
  if (env->fault_occurred)
    return ERR_RPC;

  ret = rpc_struct_read_string(rpc, rv, "handle", contact);

  xmlrpc_DECREF(rv);

  if ( ret < 0 )
    return ret;

  return ERR_SUCCESS;
}
