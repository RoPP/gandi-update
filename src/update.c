#include "gandi.h"

#define LOG_RETURN(rpc,msg)                                             \
  do {                                                                  \
    LOG_RPC(ERR_RPC == ret, LOG_ERR, *rpc, msg);                        \
    LOG_SYS(ERR_ERRNO == ret, LOG_ERR, msg);                            \
                                                                        \
    LOG_GEN(ret < 0 && ERR_RPC != ret && ERR_ERRNO != ret, LOG_ERR,     \
            "update_new_version() failed");                             \
                                                                        \
    if (0 > ret)                                                        \
      return ret;                                                       \
  } while (0)

int
update_get_version(struct rpc *rpc, char *domain, struct update *u)
{
  int ret;

  u->zone_id            = ret = rpc_zone_id(rpc, domain);
  LOG_RETURN(rpc, "update_get_version(): rpc_zone_id() failed");

  u->current_version_id = ret = rpc_current_zone_version(rpc, u->zone_id);
  LOG_RETURN(rpc, "update_get_version(): rpc_current_zone_version() failed");

  return ERR_SUCCESS;
}

int
update_new_version(struct rpc *rpc, char *domain, struct update *u)
{
  int ret;

  ret = update_get_version(rpc, domain, u);
  LOG_RETURN(rpc, "update_new_version(): update_get_version() failed");

  u->new_version_id     = ret = rpc_new_zone_version(rpc, u->zone_id);
  LOG_RETURN(rpc, "update_new_version(): rpc_new_zone_version() failed");

  return ERR_SUCCESS;
}

int
update_get_current_value(struct rpc *rpc, struct update *u, char *host, char *record_type, char **value)
{
  int ret;

  ret = rpc_value_zone_record(rpc, u->zone_id, u->current_version_id, host, record_type, value);
  LOG_RETURN(rpc, "update_get_current_value():  rpc_value_zone_record() failed");

  return ERR_SUCCESS;
}

int
update_set_record(struct rpc *rpc, struct update *u, char *host, char *record_type, char *value, int ttl)
{
  int ret;

  ret = rpc_zone_record_exists(rpc, u->zone_id, u->new_version_id, host, record_type);
  LOG_RETURN(rpc, "update_set_record(): rpc_zone_record_exists() failed");

  if (ret)
    {
      ret = rpc_delete_zone_record(rpc, u->zone_id, u->new_version_id, host, record_type);
      LOG_RETURN(rpc, "update_set_record(): rpc_delete_zone_record() failed");
    }

  ret = rpc_add_zone_record(rpc, u->zone_id, u->new_version_id, host, record_type, value, ttl);
  LOG_RETURN(rpc, "update_set_record(): rpc_add_zone_record() failed");

  return ERR_SUCCESS;
}

int
update_activate_version(struct rpc *rpc, struct update *u)
{
  int ret;

  ret = rpc_activate_zone_version(rpc, u->zone_id, u->new_version_id);
  LOG_RETURN(rpc, "update_activate_version(): rpc_activate_zone_version() failed\n");

  ret = rpc_delete_zone_version(rpc, u->zone_id, u->current_version_id);
  LOG_RETURN(rpc, "update_activate_version(): rpc_delete_zone_version() failed\n");

  return ERR_SUCCESS;
}
