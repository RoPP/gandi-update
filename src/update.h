#ifndef __UPDATE_H
#define __UPDATE_H

#include "rpc.h"

struct update {
  int zone_id;
  int current_version_id;
  int new_version_id;
};

int update_get_version(struct rpc *rpc, char *domain, struct update *u);
int update_new_version(struct rpc *rpc, char *domain, struct update *u);
int update_get_current_value(struct rpc *rpc, struct update *u, char *host, char *record_type, char **value);
int update_set_record(struct rpc *rpc, struct update *u, char *host, char *record_type, char *value, int ttl);
int update_activate_version(struct rpc *rpc, struct update *u);

#endif
