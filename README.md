Gandi-Update
============

Compiling
---------

Install dependencies
```bash
$ sudo apt-get install libxmlrpc-core-c3 libxmlrpc-core-c3-dev
```

Compile

```bash
$ ./configure
$ make
```

Installing
----------

As root, install the daemon

```bash
$ make install
```

Copy example config file from the documentation directory and write in your gandi xml-rpc api key

```bash
$ cp /usr/local/share/doc/gandi-update/gandi-update.conf /etc/
```

Copy the init.d script

```bash
$ cp /usr/local/share/doc/gandi-update/init.d-script-debian /etc/init.d/gandi-update
$ chmod +x /etc/init.d/gandi-update
```

Configure an unprivilegied user

(From http://www.debian.org/doc/manuals/securing-debian-howto/ch9.en.html)

```bash
$ addgroup --quiet --system gandi
$ adduser --quiet --system --ingroup gandi --no-create-home --disabled-password gandi
$ mkdir /var/run/gandi/
$ chown gandi.gandi /var/run/gandi/
$ usermod -c "gandi-update" -d "/var/run/gandi/" -g gandi gandi
```


Start at boot time

```bash
$ update-rc.d gandi-update defaults
```

Then check everything is fine

```bash
$ ps -eo pid,user,group,args --sort user | grep gandi
21368 gandi    gandi    /usr/local/sbin/gandi-update --daemon --user gandi

$ tail /var/log/syslog
[...]
Jul 28 17:38:08 pc gandi-update[21368]: Checking update for test.example.com A ppp0
Jul 28 17:38:09 pc gandi-update[21368]: Nothing to do for test.example.com A 127.0.0.1
[...]
```
