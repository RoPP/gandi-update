#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

AC_PREREQ([2.68])
AC_INIT([gandi-update], [1.1], [alenvers@alenvers.eu])
AM_INIT_AUTOMAKE([no-dist-gzip dist-xz tar-ustar])
AM_SILENT_RULES([yes])

AC_CONFIG_MACRO_DIR([m4])
AC_CONFIG_SRCDIR([src/gandi-update.c])
AC_CONFIG_HEADERS([config.h])

# Checks for programs.
AC_PROG_CC
AC_PROG_CC_STDC
AC_PROG_LIBTOOL
AM_PROG_CC_C_O

# Configure options
AC_ARG_ENABLE([test-api-key],
	[AS_HELP_STRING([--enable-test-api-key],
	[disable test api key @<:@default: no@:>@])],
	[test_api_key=${enableval}], [test_api_key=no])

if test "x${test_api_key}" != "xno"; then
   AC_DEFINE_UNQUOTED([TEST_API_KEY], "${test_api_key}", [Test api key])
fi

PKG_PROG_PKG_CONFIG

# Check for systemd
AC_ARG_WITH([systemdsystemunitdir],
            AS_HELP_STRING([--with-systemdsystemunitdir=DIR], [Directory for systemd service files]),
            [],
            [with_systemdsystemunitdir=$($PKG_CONFIG --variable=systemdsystemunitdir systemd)])
if test "x$with_systemdsystemunitdir" != "xno"; then
	AC_SUBST([systemdsystemunitdir], [$with_systemdsystemunitdir])
fi
AM_CONDITIONAL(HAVE_SYSTEMD, [test -n "$systemdsystemunitdir"])

# Checks for header files.
AC_CHECK_HEADERS([limits.h stdint.h stdlib.h string.h unistd.h])

AC_CHECK_HEADERS([xmlrpc.h xmlrpc_client.h],,[AC_MSG_ERROR([
------------------------------------------------

 <xmlrpc.h> or <xmlrpc_client.h> header missing

 For debian based
 $ apt-get install libxmlrpc-core-c3-dev
 or
 install http://xmlrpc-c.sourceforge.net/

------------------------------------------------
])])

AC_CHECK_HEADERS([curl/curl.h],,[AC_MSG_ERROR([
------------------------------------------------

 <curl/curl.h> header missing

 For debian based
 $ apt-get install libcurl3-dev

------------------------------------------------
])])


# Checks for typedefs, structures, and compiler characteristics.
#AC_C_INLINE
#AC_TYPE_OFF_T
AC_TYPE_PID_T
#AC_TYPE_SIZE_T
#AC_TYPE_SSIZE_T
#AC_TYPE_UINT16_T

# Checks for library functions.
AC_CHECK_FUNCS([strerror strdup])
AC_FUNC_FORK
#AC_FUNC_MMAP

# Search for libraries
AC_SEARCH_LIBS([xmlrpc_build_value],[xmlrpc],,[AC_MSG_ERROR([
------------------------------------------

 libxmlrpc missing

 For debian based
 $ apt-get install libxmlrpc-core-c3
 or
 install http://xmlrpc-c.sourceforge.net/

------------------------------------------
])])

AC_SEARCH_LIBS([xmlrpc_client_create], [xmlrpc_client],,[AC_MSG_ERROR([
--------------------------
 libxmlrpc_client missing
--------------------------
])])

AC_SEARCH_LIBS([xmlrpc_env_init], [xmlrpc_util],,[AC_MSG_ERROR([
--------------------------
 libxmlrpc_util missing
--------------------------
])])

AC_SEARCH_LIBS([curl_easy_init], [curl],,[AC_MSG_ERROR([
--------------------------
 libcurl missing
--------------------------
])])


AC_CONFIG_FILES([Makefile src/Makefile test/Makefile doc/Makefile data/Makefile])
AC_OUTPUT
